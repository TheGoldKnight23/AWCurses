/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 05/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/Terminal.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Exceptions.hpp>

#include <curses.h>
#include <system_error>
#include <sstream>

using namespace awcurses;
using namespace awcurses::core;
using namespace std;



Screen::Screen(const Position& position, const Size& size) : Screen(Cursor(this), position, size) {}

Screen::Screen(Cursor cursor, const Position& position, const Size& size) : _cursor(std::move(cursor)), _position(position), _size(size) {}




void Screen::show() {
    // Ensure initsrc() has been called
    core::Terminal::instance();
    // Initalise the wrapper window
    if (!top_win) {
        // Remove it if it already exists
        top_win = makeManagedWindow(newwin(_size.height(), _size.width(), _position.y(), _position.x()));
        if (!top_win) {
            throw exceptions::CursesException("Failed to create new window (Screen::show())\n"+std::to_string(_size.height())+"\n"+std::to_string(_size.width()));
        }
    }
    if (!_win) {
        // initalise the inner window, if it doesnt already exist
        _win = makeManagedWindow(subwin(top_win.get(), _size.width(), _size.width(), _position.y(), _position.x()));
        if (!_win) {
            throw exceptions::CursesException("Failed to create subwindow (Screen::show())");
        }
    }

    // Execute the instructions
    while (_instruction_queue.size() > 0) {
        // Get it from the queue
        instruction_t instruction = _instruction_queue.front();
        // Execute it
        instruction();
        // Only remove if didnt throw
        _instruction_queue.pop();
    }

    // Set setup to true as the Screen is now ready to be used for terminal IO/curses functions
    setup = true;
}


void Screen::hide() {
    if (_win) {
        _cursor.eraseAll();
        untouchwin(_win.get());
    }
    setup = false;
}


void Screen::wait() {
    stringstream buffer;
    addInstruction([this, &buffer] { _cursor.getInput(buffer); });
}


void Screen::setColour(int color_num) {
    // Set the windows pair to the one specifed
    addInstruction([this, color_num]{wbkgd(_win.get(), COLOR_PAIR(color_num));});
}

void Screen::setColourDefault() {
    // Sets the default which is colour set 1
    setColour(0);
}

void Screen::setColourSelected() {
    setColour(1);
}


void
Screen::setBorder(char top, char right, char top_right, char bottom_right, char left, char top_left, char bottom_left,
                  char bottom) {
    
    addInstruction([this, top, top_left, top_right, left, right, bottom_left, bottom_right, bottom]{
        exceptions::ErrorCode e(wborder(top_win.get(), top, top_left, top_right, left, right, bottom_left, bottom_right, bottom));
        check_err(e);
        });
}

void Screen::setBorder(char left_right, char top_bottom) {
    addInstruction([this, left_right, top_bottom]{
        exceptions::ErrorCode e(box(top_win.get(), left_right, top_bottom));
        check_err(e);
    });
}

void Screen::setLineBorder() {
    // Default curses unbroken border
    addInstruction([this]{
        exceptions::ErrorCode e(box(top_win.get(), 0, 0));
        check_err(e);
        });
}

void Screen::clearBorder() {
    // Set the border to nothing
    addInstruction([this]{
        exceptions::ErrorCode e(wborder(top_win.get(), ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '));
        check_err(e);
        });
}

void Screen::clear() {
    // Clears the screen
    addInstruction([this]{
        exceptions::ErrorCode e(wclear(_win.get()));
        check_err(e);
        exceptions::ErrorCode e2(wrefresh(_win.get()));
        check_err(e2);
    });
}

void Screen::refresh() {
    // Wraps the underlying refresh function of the WINDOW object
    addInstruction([this]{
        exceptions::ErrorCode e(wrefresh(_win.get()));
        check_err(e);
        });
}

bool Screen::is_setup() const {
    if (!_win) {
        // win is nullptr then its not setup (even if setup = true) so return false
        return false;
    }
    return setup;
}

void Screen::touch() {
    // Call the low-level curses touchwin() to actually touch the window
    addInstruction([this]{
        exceptions::ErrorCode e(touchwin(_win.get()));
        check_err(e);
        });
    
}

Screen::operator bool() {
    return setup;
}

std::unique_ptr<WINDOW, std::function<void(WINDOW*)>> Screen::makeManagedWindow(WINDOW *win) {
    // Create the custom deleter
    auto deleter = [](WINDOW* win) noexcept {
        delwin(win);
        // Dont throw in the destructor
    };
    
    // Return the new unique_ptr
    return std::unique_ptr<WINDOW, std::function<void(WINDOW*)>>(win, deleter);
}

WINDOW *Screen::curses_window() const {
    if (!is_setup()) {
        throw exceptions::InvalidScreenError("awcurses::core::Screen::curses_window()");
    } 
    return _win.get();
}

WINDOW* Screen::curses_window() {
    // Fix any show execeptions here
    if (!is_setup()) show();
    
    // Cast to const and return the const variatation
    return static_cast<const Screen*>(this)->curses_window();
}


void Screen::addInstruction(instruction_t instruction) {
    if (is_setup()) {
        // Execute it immediatly
        instruction();
    } else {
        // Add it to the instruction queue
        _instruction_queue.push(instruction);
    }
}









