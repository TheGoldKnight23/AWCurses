/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by Ash on 10/03/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Core/Exceptions.hpp>
#include <string>
#include <curses.h>

using namespace awcurses;


core::exceptions::Exception::Exception(const std::string& msg) : msg(msg) {}


const char* core::exceptions::Exception::what() const noexcept {
        return msg.c_str();
    }

std::string core::exceptions::Exception::appendMsg(const std::string& add) const  {
        return msg + add;
    }

std::string core::exceptions::Exception::prependMsg(const std::string& before) const {
        return before + msg;
    }


core::exceptions::InvalidScreenError::InvalidScreenError(const std::string& thrown_path) : LogicError(err_prefix + thrown_path) {}


core::exceptions::CursesException::CursesException(const std::string& err_msg) : RuntimeError(err_prefix + err_msg) {}


core::exceptions::CursesException 
core::exceptions::CursesException::fromErrorCode(const core::exceptions::ErrorCode& err_code) {
    switch(err_code.code) {
        
        case OK:
            throw RuntimeError("CursesException::fromErrorCode passed curses OK - meaning an error hasnt actually occured!");
        case ERR: 
            return CursesException("GENERIC ERROR (curses just returned ERR, likely a nullptr to a WINDOW* somewhere)");
    }
    throw RuntimeError(std::string("CursesException::fromErrorCode passed an unknown code!: ")+err_code);
}

