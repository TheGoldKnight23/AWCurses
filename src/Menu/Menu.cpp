/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 12/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Core/KEYS.inl>
#include <AWCurses/Core/Exceptions.hpp>
#include <utility>

#include <AWCurses/Menu/Menu.hpp>
#include <AWCurses/Menu/MenuItem.hpp>
#include <AWCurses/Core/Cursor.hpp>

using namespace awcurses::menu;
using namespace awcurses::core;
using namespace std;


void Menu::addItem(const string &title, const string &description, menu_callback callback) {
    auto item = make_unique<MenuItem>(this, title, description, next_position, item_size, false);
    item->callback = std::move(callback);
    addItem(std::move(item));
}


void Menu::addItem(const string &title, const string &description) {
    auto item = make_unique<MenuItem>(this, title, description, next_position, item_size, false);
    addItem(std::move(item));
}

void Menu::addItem(unique_ptr <MenuItem> item) {
    next_position.y(next_position.y()+new_item_height);
    items.reserve(1);
    items.emplace_back(std::move(item));
}


void Menu::post() {
    // Ensure screen is setup
    if (!_screen) _screen.show();
    
    // There are items to work with
    if (items.empty()) return;
    
    // Show all the items
    for (const auto &item : items) {
        try {
            item->show();
        }
        catch (exceptions::InvalidScreenError &err) {
            // Catch and rethrow with path
            throw exceptions::InvalidScreenError(err.prependMsg("awcurses::menu::Menu::post()->"));
        }
    }
    // Hide the cursor
    curs_set(0);
    
    // Select last item
    items[current_item_index]->select();
    // Take input from the cursor and move
    int c;
    do {
        // Show items
        for (const auto &item : items) {
            item->show();
        }
        c = items[current_item_index]->screen().cursor().getchar(false);
        switch (c) {
            case keys::down: downItem();
                break;
            case keys::up: upItem();
                break;
            case keys::enter: items[current_item_index]->triggerCallback();
                break;
            default: {}
        }
    } while (c && posted);
}

void Menu::unpost() {
    for (const auto &item : items) {
        item->hide();
    }
    _screen.hide();
    refresh();
}

void Menu::upItem() {
    // Moves up to the next item in the linked list of items
    if (current_item_index - 1 < 0) return;
    // There are items left to move
    
    // Unselected the current item
    items[current_item_index]->unselect();
    --current_item_index;
    items[current_item_index]->select();
}

void Menu::downItem() {
    // Moves down to the next item
    if (current_item_index + 1 > items.size() - 1) return;
    // Items left to move
    
    // unselect the old item
    items[current_item_index]->unselect();
    // Increase the item index
    ++current_item_index;
    items[current_item_index]->select();
}





