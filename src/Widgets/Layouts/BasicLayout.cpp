/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by Ash on 11/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Widgets/Layouts/BasicLayout.hpp>
#include <AWCurses/Widgets/Layouts/BasicLayoutItem.hpp>
#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/KEYS.inl>


using namespace awcurses::core;
using namespace awcurses::widgets::layouts;

awcurses::widgets::layouts::BasicLayout::BasicLayout(Screen *parent) : BasicLayout(parent, Size(parent->size().width() -1, parent->size().height() -1)) {}


awcurses::widgets::layouts::BasicLayout::BasicLayout(const core::Size& size) : Widget(core::Position(), size) {}

awcurses::widgets::layouts::BasicLayout::BasicLayout(Screen *parent, const core::Size& size) : Widget(parent, parent->position(), size) {}


void awcurses::widgets::layouts::BasicLayout::addItem(std::shared_ptr <GenericBasicLayoutItem> item) {
    items.emplace_back(std::move(item));
}





void awcurses::widgets::layouts::BasicLayout::select() {
    if (!_screen.is_setup()) {
        // Screen hasnt been setup yet so set it up
        _screen.show();
    }
    for (const auto &item : items) {
        // Show all the items
        item->show();
    }
    // Touch it to force it on top
    
    // Start capturing input
    _screen.cursor().captureInput();
}

void awcurses::widgets::layouts::BasicLayout::initCursor() {
    // Add escape key causing end of input
    _screen.cursor().addKeybind(keys::esc, Cursor::keybind_callback{[](int) -> bool { return true; }});
}




