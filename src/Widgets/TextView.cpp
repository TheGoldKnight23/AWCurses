/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by ash on 13/01/2020.
Copyright (c) 2020 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Widgets/TextView.hpp>

#include <utility>
#include <AWCurses/Core/Screen.hpp>

void awcurses::widgets::TextView::select() {
    if (!_screen.is_setup()) _screen.show();
    // Screen is properly setup and shouldn't throw
    
    // Set selected colour to show selection
    _screen.setColourSelected();
    
    show();
    //getCursor()->getchar(false);
    
}

void awcurses::widgets::TextView::show() {
    refresh();
}

void awcurses::widgets::TextView::setText(const std::string &set) {
    text = set;
}

std::string awcurses::widgets::TextView::getText() const {
    return text;
}

void awcurses::widgets::TextView::refresh() {
    if (!_screen) _screen.show();
    
    // Reprint text
    auto &c = _screen.cursor();
    c.resetPos();
    c.eraseAll();
    c.print(text, 0, 0, true);
    // Screen is setup so it shouldn't throw
    _screen.refresh();
}

awcurses::widgets::TextView::TextView(std::string text, const core::Position& position, const core::Size& size) : 
Widget(position, size), text(std::move(text)) {}

void awcurses::widgets::TextView::unselect() {
    if (!_screen.is_setup()) _screen.show();
    // Ensure the Screen is setup
    _screen.setColourDefault();
    _screen.refresh();
}
