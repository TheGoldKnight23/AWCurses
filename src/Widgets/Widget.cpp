/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 14/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <AWCurses/Widgets/Widget.hpp>
#include <curses.h>
#include <AWCurses/Core/Exceptions.hpp>
#include <AWCurses/Core/Screen.hpp>


using namespace awcurses::widgets;
using namespace awcurses::core;
using namespace std;


Widget::Widget(const core::Position& position, const core::Size& size) : _screen(position, size), _position(position), _size(size) {}


Widget::Widget(core::Screen *parent, const core::Position& position, const core::Size& size) : 
_screen(position, size), parent(parent)  {}


Widget::~Widget() = default;

void Widget::show() {
    _screen.show();
}









