/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by ash on 01/01/2020.
Copyright (c) 2020 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#include <gtest/gtest.h>
#include <AWCurses/Core/Screen.hpp>
#include <AWCurses/Core/Cursor.hpp>
#include <AWCurses/Core/Exceptions.hpp>
#include <curses.h>
#include <AWCurses/Core/Terminal.hpp>

using namespace std;

namespace awcurses_tests {
namespace core {
    /// Tests for exceptions thrown by the constructors
    TEST(ScreenTest, ScreenTest_Constructors_Test) {
        ASSERT_NO_THROW(awcurses::core::Screen screen());
        ASSERT_NO_THROW(awcurses::core::Screen screen1(awcurses::core::Position()));
        ASSERT_NO_THROW(awcurses::core::Screen screen2(awcurses::core::Position(), awcurses::core::Size()));
    }

    /// Tests a normal usage of Screen
    TEST(ScreenTest, ScreenTest_Usage_Test) {
        awcurses::core::Screen screen;
        screen.setColourDefault();
        ASSERT_EQ(screen.cursor().position().x(), 0);
        screen.refresh();
        screen.setLineBorder();
        screen.hide();        
    }
}
}