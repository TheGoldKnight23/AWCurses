var classWidget =
[
    [ "Widget", "classWidget.html#af303922bb82e80cf2ec5af8ff894782a", null ],
    [ "Widget", "classWidget.html#a2c94734c6d4b62afbba302bec61dd57a", null ],
    [ "~Widget", "classWidget.html#a714cf798aadb4d615f6f60a355382c02", null ],
    [ "getHeight", "classWidget.html#a3e38ff235ca376646a3ebe6aede03cda", null ],
    [ "getScreen", "classWidget.html#ae7861eac1c3f71f70bcb85ef6f1e20c2", null ],
    [ "getWidth", "classWidget.html#ae10f30f562f89a948760fcfa6c69f9aa", null ],
    [ "getX", "classWidget.html#a36e775bd4c93a4624672073a1eee198b", null ],
    [ "getY", "classWidget.html#a18e335dd3414fd5994c11f96cccac8e2", null ],
    [ "setHeight", "classWidget.html#abedaa756ceacd9045a636bba0373e332", null ],
    [ "setScreen", "classWidget.html#afdac0a13961413dd01c6fa735476cd25", null ],
    [ "setWidth", "classWidget.html#af6e9aeca44d395c81506be8f8517c8c6", null ],
    [ "setX", "classWidget.html#a55025a7cd2de1ce3953c805d45fe0c45", null ],
    [ "setY", "classWidget.html#a0061777a8f7e8e365e89ff0f739ef059", null ],
    [ "cursor", "classWidget.html#a064db00f6a04251a74e45b1f53747ead", null ],
    [ "height", "classWidget.html#a125fe2433b48b3531fa417b9b94fc499", null ],
    [ "parent", "classWidget.html#a58cfa6617bd6cba90b02fadebfe3469d", null ],
    [ "screen", "classWidget.html#ad17c80463dc36a4e65bd7f848208f7cc", null ],
    [ "width", "classWidget.html#adbead3105a6d25d18b2d6e942cd1af27", null ],
    [ "x", "classWidget.html#a060254d207ecabf9a1485d48e4f350b5", null ],
    [ "y", "classWidget.html#ab7ba53959a67aec4acb6019347066f03", null ]
];