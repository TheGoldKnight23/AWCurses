var annotated_dup =
[
    [ "awcurses", "namespaceawcurses.html", "namespaceawcurses" ],
    [ "layouts", null, [
      [ "BasicLayout", "classlayouts_1_1_basic_layout.html", "classlayouts_1_1_basic_layout" ]
    ] ],
    [ "BasicLayoutItem", "class_basic_layout_item.html", "class_basic_layout_item" ],
    [ "Callback", "class_callback.html", "class_callback" ],
    [ "CommandLine", "class_command_line.html", "class_command_line" ],
    [ "ExeCallback", "class_exe_callback.html", "class_exe_callback" ],
    [ "Form", "class_form.html", null ],
    [ "GenericBasicLayoutItem", "class_generic_basic_layout_item.html", "class_generic_basic_layout_item" ],
    [ "ListLayout", "class_list_layout.html", "class_list_layout" ],
    [ "ListLayoutRow", "class_list_layout_row.html", null ],
    [ "Screen", "class_screen.html", "class_screen" ],
    [ "TextView", "class_text_view.html", "class_text_view" ]
];