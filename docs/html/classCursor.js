var classCursor =
[
    [ "Cursor", "classCursor.html#a0de73b6b3291ecfec15242e04db17fe5", null ],
    [ "Cursor", "classCursor.html#ab254a2cf7e128098e2295e3dba1f20ec", null ],
    [ "erase", "classCursor.html#a05088120f95ee523ff505c92f3190a6b", null ],
    [ "eraseAll", "classCursor.html#ac5ff61db564ccbc591fe313a2c667029", null ],
    [ "getInput", "classCursor.html#a35bd771b40aa97c8d65f04bab2622886", null ],
    [ "getX", "classCursor.html#a32172f293dc42c6256ea95241ac1f1b6", null ],
    [ "getY", "classCursor.html#a737cccaafed3d0a03977b9c5f4cfe7cf", null ],
    [ "getZeroX", "classCursor.html#a0f03b8209b121dc6389c7ff0aa1ff767", null ],
    [ "getZeroY", "classCursor.html#ad182d4ef227f80895b85a87ac1b84aff", null ],
    [ "lockPosition", "classCursor.html#aa5d8b22011cfcdc01677038299d628ae", null ],
    [ "operator<<", "classCursor.html#a9e43599fbab049aa8bb79d7466a3b207", null ],
    [ "operator>>", "classCursor.html#a1967abb0e8214d69867e70ed5f1f5bc2", null ],
    [ "print", "classCursor.html#adb768aaaf046811ae2ee7a8d0c3f0485", null ],
    [ "print", "classCursor.html#a0761119544a277c18f0edee1135e6d25", null ],
    [ "println", "classCursor.html#abc31596402a4dbafe9676466e10b695b", null ],
    [ "resetPos", "classCursor.html#afd6c5f0a152eac820e88fcd8a63df56e", null ],
    [ "setScreen", "classCursor.html#a8dc0ed63f127b0b3f6a8750e909e8fcb", null ],
    [ "setX", "classCursor.html#a26b247a871a62c7c18e2220f23b7cef9", null ],
    [ "setY", "classCursor.html#a027e6f2a12d68616f934b2840dc64479", null ],
    [ "setZeroX", "classCursor.html#af07ac0eed4bd126fbc5a118128f80ba5", null ],
    [ "setZeroY", "classCursor.html#ada1a4976112dd02b1e8d3fe749e07099", null ]
];