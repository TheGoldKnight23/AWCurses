var namespaceawcurses_1_1widgets =
[
    [ "layouts", null, [
      [ "BasicLayout", "classawcurses_1_1widgets_1_1layouts_1_1_basic_layout.html", "classawcurses_1_1widgets_1_1layouts_1_1_basic_layout" ],
      [ "BasicLayoutItem", "classawcurses_1_1widgets_1_1layouts_1_1_basic_layout_item.html", "classawcurses_1_1widgets_1_1layouts_1_1_basic_layout_item" ],
      [ "GenericBasicLayoutItem", "classawcurses_1_1widgets_1_1layouts_1_1_generic_basic_layout_item.html", "classawcurses_1_1widgets_1_1layouts_1_1_generic_basic_layout_item" ],
      [ "ListLayout", "classawcurses_1_1widgets_1_1layouts_1_1_list_layout.html", "classawcurses_1_1widgets_1_1layouts_1_1_list_layout" ]
    ] ],
    [ "CommandLine", "classawcurses_1_1widgets_1_1_command_line.html", "classawcurses_1_1widgets_1_1_command_line" ],
    [ "InputBox", "classawcurses_1_1widgets_1_1_input_box.html", "classawcurses_1_1widgets_1_1_input_box" ],
    [ "TextView", "classawcurses_1_1widgets_1_1_text_view.html", "classawcurses_1_1widgets_1_1_text_view" ],
    [ "Widget", "classawcurses_1_1widgets_1_1_widget.html", "classawcurses_1_1widgets_1_1_widget" ]
];