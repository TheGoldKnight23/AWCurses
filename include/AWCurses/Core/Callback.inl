
/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
---------------------------------------
Created by ash on 24/01/2020
Copyright (c) Natash England-Elbro 2020
----------------------------------------
/*/

#ifndef AWCURSES_CALLBACK_INL_B44A09B5CFEB46BCAD3EFACBECA3D62B
#define AWCURSES_CALLBACK_INL_B44A09B5CFEB46BCAD3EFACBECA3D62B

#include <functional>
#include <list>

namespace awcurses {
namespace core {
namespace callbacks {

/**
 * @file Inline file for AWCurses::Core callbacks
 */

/**
 * @addtogroup Core
 * @{
 */
/**
 * @brief The base callback class template for AWCurses callbacks
 * @class Callback
 * @tparam RTR 
 * @tparam TAKE 
 */

template <typename RTR, typename ...TAKE>
class Callback {
public:
    /// Default constructor, just initialises the object
    Callback() = default;
    
    /// Constructor for setting the function object to use
    Callback(std::function<RTR(TAKE...)> func) : function(std::move(func)) {}
    
    virtual ~Callback() = default;
    
    /// The call operator, overloaded to wrap Callback::function
    virtual RTR operator()(TAKE... args) { return function(args...); }
    
    /// Conversion operator to std::function for convenience
    virtual explicit operator std::function<RTR(TAKE...)>() {
        return function;
    }
    /// Copy operator
    Callback(const Callback&) = default;
    /// Move operator
    Callback(Callback&&) = default;
    /// Copy assignment operator
    virtual Callback& operator=(const Callback&) = default;
    /// Move assignment operator
    virtual Callback& operator=(Callback&&) = default;

    /// The function object used internally by the Callback
    std::function<RTR(TAKE...)> function;
};

/// An executable callback which functions like an `int main(int, char**)` method
/**
 * @class ExeCallback
 * @details Designed to mimic the `int main(int, char**)` design of C/C++ main methods in order to provide maximum
 * flexibility in argument taking and handling results from arguments.
 * 
 * The `std::list<std::string>` passed to the callback will be as follows
 * - [0]: The `command` used to execute the callback
 * - [1...]: Any extra arguments `passed` to the callback
 * 
 * The callback should return 0 on success or a non-zero code on failure
 */
class ExeCallback: public Callback<int, const std::list<std::string>&> {
public:
    /// Function pattern for the ExeCallback to use
    using func_pattern = std::function<int(const std::list<std::string>&)>;
    
    /// Inherit constructors
    using Callback<int, const std::list<std::string>&>::Callback;
};


/** 
 * @}
*/ 

} // callbacks
} // core
} // awcurses

#endif //AWCURSES_CALLBACK_INL_B44A09B5CFEB46BCAD3EFACBECA3D62B
