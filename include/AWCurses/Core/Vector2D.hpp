/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2020. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 18/03/2020.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_CORE_VECTOR2D_HPP
#define AWCURSES_CORE_VECTOR2D_HPP


/**
 * @file Header only file for AWCurses Vector2D class
 */

namespace awcurses {
namespace core {
    
/// A class to represent anything in 2 dimensions (location, size, etc)
class Vector2D {
public:
    /// Construct the vector with two elements, first and second
    Vector2D(const int first, const int second) : _first(first), _second(second) {}

    /// Copy constructor
    Vector2D(const Vector2D&) = default;

    /// Add togehter two Vector2D objects and return a new Vector2D
    Vector2D operator+(const Vector2D& size) const {
        return Vector2D(size._first + _first, size._second + _second);
    }
    /// Add a Vector2D to the current Vector2D object
    void operator+=(const Vector2D& size) {
        _first += size._first;
        _second += size._second;
    }
    /// Minus two Vector2D objects and return a new Vector2D
    Vector2D operator-(const Vector2D& size) const {
        return Vector2D(_first - size._first, _second - size._second);
    }

    /// Reduce the size
    /**
     * Effectivly reduces the size in both _second and _first by the specifed integer value
     */
    Vector2D operator-(int val) const {
        return Vector2D(_first - val, _second - val);
    }
    /// Minus another Vector2D object from the current Vector2D object
    void operator-=(const Vector2D& size) {
        _first -= size._first;
        _second -= size._second;
    }
    /// Compare a size object with the current size object for equaility
    bool operator==(const Vector2D& size) const {
        return (size._second == _second) && (size._first == _first);
    }

protected:
    /// The two elements in the vector
    int 
        _first,
        _second;

};


} // namespace core

    
} // namespace awcurses





#endif // AWCURSES_CORE_VECTOR2D_HPP


