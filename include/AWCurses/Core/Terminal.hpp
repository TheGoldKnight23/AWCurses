/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

#ifndef AWCURSES_TERMINAL_H
#define AWCURSES_TERMINAL_H

#include <map>
#include <memory>
#include <vector>
#include <mutex>
#include <atomic>

/**
 * @file Header file for the Terminal class
 */

/**
 * @defgroup Core
 * @brief The core library of AWCurses
 * @details The core library of AWCurses, containing the classes and functions needed by all of AWCurses modules. Can
 * also be used as a standalone library for bare bones AWCurses functionality.
 */

/**
 * @namespace awcurses::core
 * @brief Contains the core functionality of AWCurses
 * @ingroup Core
 */
namespace awcurses {
namespace core {
class Screen;

/**
 * @class Terminal
 * @brief Represents the user's terminal
 * @details The Terminal class is an optional memory management solution for AWCurses. It uses the singleton design
 * pattern and manages the lifetimes of the curses session through it's lifetime (When created - curses starts, when
 * destructed - curses ends). Completely optional. start() method is the only required one, it being used to start the
 * curses session 'manually' as it were.
 * @ingroup Core
 */
class Terminal {
public:
    void operator=(const Terminal&) = delete;
    Terminal(const Terminal&) = delete;
    
    /**
     * @brief Ends the terminal session. Does not end the AWCurses session.
     * @details Ends the curses session but does not deconstruct the Terminal object. This allows a successive call
     * to start() to restart the terminal session.
     */
    static void end();
    
    /// Get the currently instanced Terminal object
    static Terminal& instance();
    
    /// Ensures curses is setup (useful for Screen for example)
    static void ensureCursesSetup();
    
    /// Get the list of Screen objects managed by the Terminal
    [[nodiscard]] const std::vector<std::unique_ptr<Screen>>& screens() const;
    
    /// Add a Screen to the Terminal, making it managed by it's lifetime
    void addScreen(std::unique_ptr<Screen> screen);
    
    ~Terminal();

private:
    /// Stores the Screen objects managed by the Terminal
    std::vector<std::unique_ptr<Screen>> _screens;
    /// Whether the curses session is setup
    static std::atomic_bool _curses_setup;


    Terminal();
    
    /**
     * @brief Starts the curses session and instances the initial screen
     * @return Returns an error code
     * @throws std::runtime_error() if the terminal lacks colours
     */
    static void start();
};
}
}


#endif //AWCURSES_TERMINAL_H
