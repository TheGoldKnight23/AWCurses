/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */

/*/
Created by Ash on 10/03/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_EXCEPTIONS_INL_104195A58CFB4011BBD328310FBD9F11
#define AWCURSES_EXCEPTIONS_INL_104195A58CFB4011BBD328310FBD9F11

#include <stdexcept>

/**
 * @file Inline file for custom AWCurses exceptions
 */

/**
 * @defgroup Exceptions
 * @brief The exceptions thrown by AWCurses components
 * @details The custom exceptions for AWCurses, thrown primarily by awcurses::core, They are all contained within one
 * header file (Exceptions.inl) within the namespace awcurses::core::exceptions.
 *
 * This mini header-only library is part of AWCurses::Core (awcurses::core) and will be installed along with it in
 * any installers.
 */

/// Contains the custom exceptions thrown by AWCurses
namespace awcurses {
namespace core {
namespace exceptions {

/// Prefix for all exceptions thrown by AWCurses
constexpr auto err_prefix = "[AWCURSES ERROR]: ";

/// Base exception class for all AWCurses exceptions
/**
 * @details All exceptions within AWCurses inherit from this class (like std::exception), this allows easy 'catchall
 * AWCurses'
 * with a reference to this class.
 * @ingroup Exceptions
 */
class Exception : public std::exception {
public:
    /// Constructor for the exception, takes a message to print out
    explicit Exception(const std::string &msg);
    
    /// Returns the error message
    [[nodiscard]] const char *what() const noexcept override;
    
    /// Append to the error message
    /**
     * @param add: The string to add to the message
     * @return The old message + the added message
     */
    [[nodiscard]] virtual std::string appendMsg(const std::string &add) const;
    /// Prepend to the error message
    /**
     * @param before
     * @return The added message + the old message
     */
    [[nodiscard]] virtual std::string prependMsg(const std::string &before) const;

protected:
    /// Message returned by what()
    std::string msg;
};

/**
 * @class ErrorCode
 * @brief A type for ErrorCodes returned by the curses C API
 * @details Generic, expandable type, designed to provide a self-documenting way of dealing with the curses
 * C API. Supports implicit construction from `int` and implicit conversion to `int`
 */
class ErrorCode {
public:
    /// Default constructor
    ErrorCode() = default;
    /// Constructor with an error code
    /**
     * Supports implicit construction from `int`
     */
    ErrorCode(int code) : code(code) {}

    /// Defaulted copy constructor
    ErrorCode(const ErrorCode&) = default;

    /// Defaulted copy operator
    ErrorCode& operator=(ErrorCode&) = default;

    /// Cstd::error_codeonvert to an `int`
    /**
     * Supports implicit conversion to `int`
     */
    operator int() const{
        return code;
    }
    /// Implicit conversion to std::string operator
    operator std::string() const{
        return std::to_string(code);
    }
    /// Explicit bool operator to check its and OK code
    explicit operator bool() const{
        return code == 0;
    }

    /// The code held by the ErrorCode object
    int code;
};

inline std::string operator+(const std::string& str, const ErrorCode& err_code) {
    return str+err_code.operator std::string();
}

/// Extension of the std::logic_error class
/**
 * Base class for errors which could theoretically be averted before runtime
 */
class LogicError: public Exception {
public:
    /// Inherit constructors
    using Exception::Exception;
};

/// Extension of the std::runtime_error class
/**
 * Base class for errors which are the result of runtime conditions and cannot be foreseen and/or avoided beforehand
 */
class RuntimeError: public Exception {
public:
    /// Inherit constructors
    using Exception::Exception;
};

/// Thrown whenever a Screen object is improperly invalid
    /**
     * @class InvalidScreenError
     * @details For example:
     * - Calling Cursor::print() before its Screen's Screen::show() method has been called
     * - Calling Screen::refresh() before calling Screen::show()
     * - Calling Screen::clear() before calling Screen::show()
     *
     * Basically, thrown whenever a Screen is asked to act in a way it cannot before being setup by calling Screen::show()
     * @ingroup Exceptions
     */
    class InvalidScreenError : public LogicError {
    public:
        /// Prefix for InvalidScreenException, the thrown_path is appended to this
        static constexpr auto err_prefix = "Invalid Screen. A Screen object has not been properly setup before being"
                                        " used (setup with Screen::show(). Thrown in: ";
        /// Constructor, takes thrown path and displays it as In:
        /**
         * @param thrown_path: The namespace path (`i.e awcurses::core::Screen`) of the thrower
         */
        explicit InvalidScreenError(const std::string &thrown_path);
    };

/// Thrown whenever an exception or error happens as a result of the underlying curses library
/**
 * @class CursesException
 * @details Since the curses library lacks exceptions and reports errors either through returned values or by simply
 * leaving nullptr references (ew), this exception is thrown by AWCurses whenever it detects an error from curses. I
 * say detects as this could be the result of a straight up error code returned, or could be the result of checking
 * for a nullptr after failing to create a new WINDOW.
 * @ingroup Exceptions
 */
class CursesException : public RuntimeError {
public:
    /// Default message for CursesException
    static constexpr auto err_prefix = "[Curses Exception]: Something went wrong with the underlying curses library. ";
    
    /// Default Constructor, just prints the default CursesException error message
    CursesException() : CursesException("") {}
    
    /// Constructor, takes an error message and adds it to CursesException::err_prefix before passing it to Exception
    /**
     * @param err_msg: The error message to be appended to CursesException::err_prefix
     */
    explicit CursesException(const std::string &err_msg);

    /// Returns a CursesException with the appropriate error code 
    static CursesException fromErrorCode(const ErrorCode& err_code);

};

/// Checks if the the passed error is a non-zero code and throws the appropriate CursesException if it is not
inline void check_err(const exceptions::ErrorCode& e) {
    if (!e) {
            throw exceptions::CursesException::fromErrorCode(e);
    }
}

/// General base class for display errors which could logically be avoided
/**
 * @class LogicDisplayError
 * @details This class tree of errors are types of errors which could _theoretically_ be avoided and are likely caused by
 * incorrect logic rather than some runtime error.
 *
 * An example of this type of error is OutOfBoundsException, which is thrown if something moves out of the
 * bounds of the terminal. i.e Cursor::moveX() moving the Cursor object below 0 in the x direction, which is out of
 * the terminal boundaries.
 *
 * These errors inherit directly from Exception as they are sometimes RuntimeErrors, sometimes LogicErrors
 * @ingroup Exceptions
 */
class LogicDisplayError : public Exception {
public:
    // Inherit constructors
    using Exception::Exception;
};

/// Thrown when trying to display, or move an object, outside of the terminal's boundaries
/**
 * @class OutOfBoundsException
 * @details This exception is primarily thrown by Cursor (as you might expect, considering it manages most terminal IO).
 *
 * A classic example of when this can be thrown is Cursor::moveX() and Cursor::moveY(), they will both throw this
 * exception if their new x or y values are less than 0, as (0, 0) is the lower/upper limit of the terminal's grid.
 *
 * Inherits from RuntimeError as can happen when the users terminal is not large enough or its size is miscalculated
 * @ingroup Exceptions
 */
class OutOfBoundsException : public LogicDisplayError, public RuntimeError {
public:
    /// Signature which is added to exceptions in this class tree
    static constexpr auto signature = "Out Of Bounds Exception - ";
    
    /// Default constructor. Adds its signature to the exception and passes it on
    explicit OutOfBoundsException(const std::string &msg) : LogicDisplayError(signature + msg), RuntimeError(signature + msg) {}
};

} // exceptions
} // core
} // awcurses


#endif //AWCURSES_EXCEPTIONS_INL_104195A58CFB4011BBD328310FBD9F11