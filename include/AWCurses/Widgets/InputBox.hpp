/*/
Created by Ash on 09/01/2020
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_WIDGETS_INPUTBOX_HPP
#define AWCURSES_WIDGETS_INPUTBOX_HPP

/**
 * @file Header file for InputBox
 */

#include <sstream>
#include <AWCurses/Widgets/Widget.hpp>


namespace awcurses {
namespace widgets {
/// Stylised input box for a user to enter things into
/**
 * @ingroup Widgets
 */
class InputBox : public Widget {
public:
    // Inherit constructors
    using Widget::Widget;
    
    /// Overriden method from Widget
    void show() override;
    
    /// Select the InputBox for data entry
    virtual void select();
    
    /// Unselect the InputBox
    virtual void unselect();

protected:
    /// Contains the text entered into the InputBox
    std::stringstream contents;
    
};
}
}

#endif //AWCURSES_WIDGETS_INPUTBOX_HPP
