/*/
Created by ash on 13/01/2020.
Copyright (c) 2020 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_TEXTVIEW_HPP_BE963B728B4B4C7191785AA5AD6D0409
#define AWCURSES_TEXTVIEW_HPP_BE963B728B4B4C7191785AA5AD6D0409

/**
 * @file Header file for TextView widget
 */

#include <AWCurses/Widgets/InputBox.hpp>
#include <AWCurses/Widgets/Widget.hpp>

namespace awcurses {
namespace widgets {

/// Widget for displaying static text which cannot be edited
/**
 * @ingroup Widgets
 */
class TextView : public Widget {
public:
    // Inherit constructors
    using Widget::Widget;
    
    /// Construct the TextView with text set
    /**
     * This will construct the TextView with the text argument passed, recommended constructor for most use cases
     * @param text: The text for the TextView to display
     * @param x: The inital x of the TextView
     * @param y: The initial y of the TextView
     * @param width: The initial width of the TextView
     * @param height: The initial height of the TextView
     */
    TextView(std::string text, const core::Position& position, const core::Size& size = core::Size());
    
    /// Overridden show which just shows and updates the widget
    void show() override;
    
    /// Select the widget
    /**
     * This method selects the widget, bringing it to the front of the terminal stack and highlighting it in the
     * default selected colour
     */
    virtual void select();
    
    /// Unselect the widget
    /**
     * This method will unselect the widget, removing its highlighting and allowing it to sent to the back
     */
    virtual void unselect();
    
    /// Set the text of the widget
    /**
     * @param set: The text to set
     * @attention: This change will not take effect until a call to TextView::refresh()
     */
    virtual void setText(const std::string &set);
    
    /// Get the text displayed by the widget
    /**
     * @return: The text displayed by the view
     */
    [[nodiscard]] virtual std::string getText() const;
    
    /// Refresh the TextView
    /**
     * Causes queued changes to appear and brings the TextView to the top of the terminal display stack
     */
    virtual void refresh();

protected:
    /// The text for the widget to display
    std::string text;
};
}
} // awcurses::widgets

#endif //AWCURSES_TEXTVIEW_HPP_BE963B728B4B4C7191785AA5AD6D0409
