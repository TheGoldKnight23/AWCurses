/*
 * ******************************************************************************
 *  * AWCurses Copyright (c) 2019. Natasha England-Elbro
 *  * This program is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published
 *  * by the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  *****************************************************************************
 *
 */
/*/
Created by ash on 12/12/2019.
Copyright (c) 2019 Natasha England-Elbro under the GNU GPLv3,
details found in the LICENSE.md file attached
/*/

#ifndef AWCURSES_MENU_HPP
#define AWCURSES_MENU_HPP

#include <vector>
#include <functional>
#include <memory>
#include <curses.h>
#include <AWCurses/Widgets/Widget.hpp>

/**
 * @file Header file for the Menu class
 */


/**
 * @defgroup Menu
 * @brief %Menu extension library for AWCurses
 * @details Extension library for AWCurses which provides menu abilities. Details under awcurses::menu Library
 * documentation
 */

namespace awcurses {
namespace core {
class Screen;
}
}


/**
 * @namespace awcurses::menu
 * @brief Contains the %Menu extension library
 * @ingroup Menu
 */

namespace awcurses {
namespace menu {
class MenuItem;

/**
* @class Menu
* @brief Provides menu capabilities
* @details Menu class - Provides a menu 'widget'. Performs a similar function to the curses 'menu' library
* @ingroup Menu
* @namespace awcurses::menu
*/
class Menu : public widgets::Widget {
public:
    /// The function format required for MenuItem callbacks
    using menu_callback = std::function<void()>;
    
    /// Default constructor, assumes the full Screen and takes whole terminal
    Menu() = default;
    
    // Inherit constructors
    using widgets::Widget::Widget;
    
    /**
     * @brief Post the menu
     * @throws exceptions::InvalidScreenError
     */
    virtual void post();
    
    /**
     * @brief Unpost the menu
     * @throws exceptions::InvalidScreenError
     */
    virtual void unpost();
    
    // Item adders
    
    /**
     * @brief Add an item to the menu
     * @param title: Title for the new item
     * @param description: Description for the new item
     * @param callback: Callback for the new item
     */
    void addItem(const std::string &title, const std::string &description, menu_callback callback);
    
    
    /**
     * @brief Add an item to the menu
     * @param item: Item to add to the menu
     */
    void addItem(std::unique_ptr <MenuItem> item);
    
    /**
     * @brief Add an item to the menu
     * @param title: Title for the new item
     * @param description: Description for the new item
     */
    void addItem(const std::string &title, const std::string &description);
    
    // Movement methods
    /**
     * @brief Move up an item in the menu
     */
    void upItem();
    
    /**
     * @brief Move down an item in the menu
     * @return Error code
     */
    void downItem();
    
    bool
    /// Whether there is a border around MenuItem's in the Menu
            using_border = false,
    /// Whether the Menu is posted
    /**
     * Changing this to false prevents the Menu from locking the terminal with input loops
     */
            posted       = true;
    /// The size for all items in the Menu
    core::Size item_size = core::Size(
        /// The width of all (new) items in the Menu
            COLS,
            new_item_height
    );
    
    /// The vector of MenuItem's used by Menu
    std::vector <std::unique_ptr<MenuItem>> items;

protected:

    int current_item_index = 0, 
    /// The height of all (new) items in the Menu
    new_item_height = 1 + ((LINES - 1) / 7);

        /// The next position for an item in the Menu
    core::Position next_position;
};
}
} // awcurses::menu


#endif //AWCURSES_MENU_HPP
